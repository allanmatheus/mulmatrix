/* This is a helper for generating test data quickly.
 */
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

void usage(const char *progname);

void write_random(FILE *f, int nrows, int ncols, int seed);

void write_identity(FILE *f, int nrows, int ncols);

int main(int argc, char **argv)
{
	int id = 0;
	int seed = 0;
	int nrows = 10;
	int ncols = 10;
	FILE *output = stdout;
	char *filename = NULL;

	int op;
	while ((op = getopt(argc, argv, "him:n:s:f:")) != -1) {
		switch (op) {
		case 'i':
			id = 1;
			break;

		case 'm':
			nrows = atoi(optarg);
			break;

		case 'n':
			ncols = atoi(optarg);
			break;

		case 's':
			seed = atoi(optarg);
			break;

		case 'f':
			filename = optarg;
			break;

		case 'h':
			usage(argv[0]);
			exit(0);

		default:
			usage(argv[0]);
			exit(1);
		}
	}

	if (nrows < 1 || ncols < 1) {
		fprintf(stderr, "Matrix size must be at least 1x1\n");
		exit(2);
	}

	if (filename != NULL) {
		output = fopen(filename, "w");
		if (output == NULL) {
			perror("fopen");
			exit(2);
		}
	}

	if (id) {
		write_identity(output, nrows, ncols);
	} else {
		write_random(output, nrows, ncols, seed);
	}

	fclose(output);
	return 0;
}

void usage(const char *progname)
{
	fprintf(stderr, "usage: %s [-h] [-i] [-f OUTPUT_FILE]"
			"[-m ROWS] [-n COLS] [-s RANDOM_SEED]\n",
			progname);
	fprintf(stderr, "-i Identity matrix instead of random.\n");
	fprintf(stderr, "-h Shows this help message.\n");
	fprintf(stderr, "-m Number of rows, 10 by default.\n");
	fprintf(stderr, "-n Number of columns, 10 by default.\n");
	fprintf(stderr, "-f Output file, defaults to stdout.\n");
}

void write_random(FILE *f, int nrows, int ncols, int seed)
{
	srand(seed);
	fprintf(f, "%d %d\n", nrows, ncols);
	for (int i = 0; i < nrows; i++) {
		for (int j = 0; j < ncols; j++) {
			char *pattern = j + 1 == ncols ? "%d\n" : "%d ";
			fprintf(f, pattern, rand());
		}
	}
}

void write_identity(FILE *f, int nrows, int ncols)
{
	fprintf(f, "%d %d\n", nrows, ncols);
	for (int i = 0; i < nrows; i++) {
		for (int j = 0; j < ncols; j++) {
			char *pattern = j + 1 == ncols ? "%d\n" : "%d ";
			fprintf(f, pattern, i == j);
		}
	}
}
