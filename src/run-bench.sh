#!/bin/bash

MULMATRIX=bin/mulmatrix
SIZES="1000 5000 10000"
NWORKERS="2 4"
MODES="t p"

for size in $SIZES ; do
    echo "# $size #"

    mtx_a="data/id-${size}.mat"
    mtx_b="data/rand-${size}.mat"

    echo "## s ##"
    {
        time $MULMATRIX -a "$mtx_a" -b "$mtx_b" -c data/out-s-${size}.mat ;
    } 2>>data/out-s-${size}.log
    diff -q data/rand-${size}.mat data/out-s-${size}.mat

    for mode in $MODES ; do
        for n in $NWORKERS ; do
            echo "## $mode $n ##"
            {
                time $MULMATRIX "-$mode" $n -a "$mtx_a" -b "$mtx_b" -c data/out-${mode}${n}-${size}.mat ;
            } 2>>data/out-${mode}${n}-${size}.log
            diff -q data/rand-${size}.mat data/out-${mode}${n}-${size}.mat
        done
    done
done
