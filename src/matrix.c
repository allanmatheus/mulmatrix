#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "matrix.h"

int matrix_move(struct matrix *self, struct matrix *other)
{
	if (self == NULL || other == NULL) {
		return -1;
	}

	matrix_free(self);

	self->v = other->v;
	self->nrows = other->nrows;
	self->ncols = other->ncols;

	other->v = NULL;
	other->nrows = 0;
	other->ncols = 0;
	return 0;
}

int matrix_copy(struct matrix *self, struct matrix *other)
{
	if (self == NULL || other == NULL) {
		return -1;
	}

	matrix_free(self);

	self->v = memmove(self->v, other->v, other->nrows * other->ncols);
	self->nrows = other->nrows;
	self->ncols = other->ncols;
	return 0;
}

int matrix_init(struct matrix *self, int nrows, int ncols)
{
	if (self == NULL) {
		return -1;
	}

	int len = nrows * ncols;

	if (len > 0) {
		self->v = calloc(len, sizeof(int));
		if (self->v == NULL) {
			return -1;
		}

		self->nrows = nrows;
		self->ncols = ncols;

	} else {
		self->v = NULL;
		self->nrows = 0;
		self->ncols = 0;
	}

	return 0;
}

void matrix_free(struct matrix *self)
{
	if (self->v != NULL) {
		free(self->v);
		self->v = NULL;
	}
	self->nrows = 0;
	self->ncols = 0;
}

int *matrix_at(struct matrix *self, int i, int j)
{
	if (self == NULL || i < 0 || j < 0) {
		return NULL;
	}
	return &(self->v[self->ncols * i + j]);
}

void display_dim(struct matrix *c)
{
	fprintf(stderr, "%dx%d\n", c->nrows, c->ncols);
}

int matrix_can_multiply(struct matrix *C, struct matrix *A, struct matrix *B)
{
	if (C == NULL || A == NULL || B == NULL) {
		fprintf(stderr, "null matrix\n");
		return 0;
	}

	if (A->ncols != B->nrows) {
		fprintf(stderr, "bad input\n");
		display_dim(A);
		display_dim(B);
		return 0;
	}

	if (A->nrows != C->nrows || C->ncols != B->ncols) {
		fprintf(stderr, "bad output\n");
		display_dim(A);
		display_dim(B);
		display_dim(C);
		return 0;
	}

	return 1;
}

int matrix_mul(struct matrix *C, struct matrix *A, struct matrix *B)
{
	if (!matrix_can_multiply(C, A, B)) {
		return -1;
	}

	for (int i = 0; i < C->nrows; i++) {
		for (int j = 0; j < C->ncols; j++) {
			int *c = matrix_at(C, i, j);
			*c = 0.0;

			for (int k = 0; k < A->ncols; k++) {
				int *a = matrix_at(A, i, k);
				int *b = matrix_at(B, k, j);
				*c += *a * *b;
			}
		}
	}

	return 0;
}
