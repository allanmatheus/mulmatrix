.SILENT:

CC       := gcc #clang-6.0
CFLAGS   := -std=c99 -pedantic -Wall -Wextra -Werror -pthread #-O4
CPPFLAGS :=
LD       := ${CC}
LDLIBS   := -lrt
LDFLAGS  := -pthread

PROGS := bin/genmatrix bin/mulmatrix

genmatrix_SRCS = src/genmatrix.c
mulmatrix_SRCS = src/matrix.c \
		 src/matrix_io.c \
		 src/matrix_mt.c \
		 src/matrix_mp.c \
		 src/mulmatrix.c

genmatrix_OBJS = ${genmatrix_SRCS:.c=.o}
mulmatrix_OBJS = ${mulmatrix_SRCS:.c=.o}

OBJS = ${genmatrix_OBJS} ${mulmatrix_OBJS}

all: ${PROGS}

bin/genmatrix: ${genmatrix_OBJS}
	@echo "CCLD $@"
	@mkdir -p bin
	${LD} ${LDFLAGS} -o $@ $^ ${LDLIBS}

bin/mulmatrix: ${mulmatrix_OBJS}
	@echo "CCLD $@"
	@mkdir -p bin
	${LD} ${LDFLAGS} -o $@ $^ ${LDLIBS}

.c.o:
	@echo "CC $@"
	${CC} ${CPPFLAGS} ${CFLAGS} -c -o $@ $<

clean:
	rm -f ${PROGS} ${OBJS}

.PHONY: all clean
