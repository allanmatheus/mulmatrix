#ifndef MATRIX_H
#define MATRIX_H

#include <stdio.h>

/**
 * struct matrix - Stores the matrix data.
 *
 * Use ``matrix_*`` functions.
 *
 * :note: All matrix data as a vector.  Use ``v[ncols*i+j]`` to access items
 * at ``(i, j)``.
 */
struct matrix {
	int *v;
	int nrows;
	int ncols;
};

/**
 * matrix_init - Initializes and allocate space for the matrix.
 *
 * :returns: ``0`` for success, ``-1`` otherwise.
 */
int matrix_init(struct matrix *self, int nrows, int ncols);

/**
 * matrix_free - Releases space for internal data.
 */
void matrix_free(struct matrix *self);

/**
 * matrix_move - Moves data from ``other`` to ``self``.  After it returns,
 * data previously in ``self`` is no longer accessible and data previously in
 * ``other`` can only be accessed through ``self``.
 *
 * :returns: ``0`` for success, ``-1`` otherwise.
 */
int matrix_move(struct matrix *self, struct matrix *other);

/**
 * matrix_copy - Copies data from ``other`` to ``self``.  After it returns,
 * data previously in ``self`` is no longer accessible.
 *
 * :returns: ``0`` for success, ``-1`` otherwise.
 */
int matrix_copy(struct matrix *self, struct matrix *other);


/**
 * matrix_at - Access the ``(i, j)`` position of this matrix.
 *
 * :returns: the address for the desired matrix cell, ``NULL`` otherwise.
 */
int *matrix_at(struct matrix *self, int i, int j);

/**
 * matrix_mul - Multiplies matrices.  Concretely, it calculates ``C=A*B``.
 * After it returns, data previously in ``C`` is no longer accessible.
 *
 * :returns: the address for the desired matrix cell, ``NULL`` otherwise.
 */
int matrix_mul(struct matrix *C, struct matrix *A, struct matrix *B);

/**
 * matrix_from_file - Loads a matrix from a readable file.
 *
 * :returns: ``0`` for success, ``-1`` otherwise.
 */
int matrix_from_file(struct matrix *self, FILE *in);

/**
 * matrix_to_file - Stores a matrix into a writable file.
 *
 * :returns: ``0`` for success, ``-1`` otherwise.
 */
int matrix_to_file(struct matrix *self, FILE *out);

/**
 * matrix_can_multiply - Verifies if C=A*B is possible..
 *
 * :returns: Boolean, ``1`` means can multiply.
 */
int matrix_can_multiply(struct matrix *C, struct matrix *A, struct matrix *B);

/**
 * matrix_tmul - Parallel matrix multiplication with ``pthreads(7)``.
 *
 * :returns: ``0`` for success, ``-1`` otherwise.
 */
int matrix_tmul(struct matrix *C, struct matrix *A, struct matrix *B,
		int nworkers);

/**
 * matrix_pmul - Parallel matrix multiplication with ``fork(2)``.
 *
 * :returns: ``0`` for success, ``-1`` otherwise.
 */
int matrix_pmul(struct matrix *C, struct matrix *A, struct matrix *B,
		int nworkers);

#endif /* MATRIX_H */
