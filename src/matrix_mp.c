#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#include <unistd.h>
#include <sys/wait.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "matrix.h"

struct pmul_slice {
	struct matrix C;
	struct matrix *A;
	struct matrix *B;

	int r_off;
	int r_end;

	pid_t pid;
};

size_t matrix_size(struct matrix *m)
{
	return sizeof(int) * m->nrows * m->ncols;
}

int matrix_shm_load(struct matrix *self, int nrows, int ncols, int fd)
{
	self->nrows = nrows;
	self->ncols = ncols;
	self->v = mmap(NULL, matrix_size(self), PROT_READ | PROT_WRITE,
			MAP_SHARED, fd, 0);
	if (self->v == MAP_FAILED) {
		perror("mmap");
		return -1;
	}
	return 0;
}

void matrix_shm_unload(struct matrix *self)
{
	size_t sz = matrix_size(self);
	munmap(self->v, sz);
}

void pmul_worker(struct pmul_slice *s)
{
	struct matrix *A = s->A;
	struct matrix *B = s->B;
	struct matrix *C = &s->C;

	for (int i = s->r_off; i < s->r_end; i++) {
		for (int j = 0; j < C->ncols; j++) {
			int *c = matrix_at(C, i, j);
			*c = 0.0;

			for (int k = 0; k < A->ncols; k++) {
				int *a = matrix_at(A, i, k);
				int *b = matrix_at(B, k, j);
				*c += *a * *b;
			}
		}
	}
}

int matrix_pmul(struct matrix *C, struct matrix *A, struct matrix *B,
		int nworkers)
{
	if (!matrix_can_multiply(C, A, B)) {
		return -1;
	}

	size_t sz = matrix_size(C);

	const char *name = "/MTX_MUL";
	int shm_fd = shm_open(name, O_CREAT | O_RDWR, 0666);
	ftruncate(shm_fd, sz);

	struct pmul_slice *slices = calloc(nworkers,
			sizeof(struct pmul_slice));

	const int r_div = C->nrows / nworkers;
	const int r_rem = C->nrows % nworkers;

	for (int i = 0; i < nworkers; i++) {
		struct pmul_slice *s = &slices[i];

		s->A = A;
		s->B = B;

		s->r_off = i * r_div;
		s->r_end = (i + 1) * r_div;
		s->r_end += (i + 1 == nworkers) ? r_rem : 0;

		if ((s->pid = fork()) == 0) {
			s->pid = getpid();
			int load = matrix_shm_load(&s->C, C->nrows, C->ncols,
					shm_fd);
			if (load == -1) {
				fprintf(stderr, "error on %d\n", s->pid);
				exit(1);
			}
			pmul_worker(s);
			matrix_shm_unload(&s->C);
			exit(0);
		}
	}

	for (int i = 0; i < nworkers; i++) {
		waitpid(slices[i].pid, NULL, 0);
	}

	free(slices);

	struct matrix R;
	matrix_shm_load(&R, C->nrows, C->ncols, shm_fd);
	for (int i = 0; i < C->nrows; i++) {
		for (int j = 0; j < C->ncols; j++) {
			*matrix_at(C, i, j) = *matrix_at(&R, i, j);
		}
	}
	matrix_shm_unload(&R);

	close(shm_fd);
	shm_unlink(name);

	return 0;
}
