#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "matrix.h"

struct tmul_slice {
	struct matrix *C;
	struct matrix *A;
	struct matrix *B;

	int r_off;
	int r_end;

	pthread_t tid;
};

void *tmul_worker(void *arg)
{
	struct tmul_slice *s = (struct tmul_slice *)arg;
	struct matrix *C = s->C;
	struct matrix *A = s->A;
	struct matrix *B = s->B;

	for (int i = s->r_off; i < s->r_end; i++) {
		for (int j = 0; j < C->ncols; j++) {
			int *c = matrix_at(C, i, j);
			*c = 0.0;

			for (int k = 0; k < A->ncols; k++) {
				int *a = matrix_at(A, i, k);
				int *b = matrix_at(B, k, j);
				*c += *a * *b;
			}
		}
	}

	return NULL;
}

int matrix_tmul(struct matrix *C, struct matrix *A, struct matrix *B,
		int nworkers)
{
	if (!matrix_can_multiply(C, A, B)) {
		return -1;
	}

	struct tmul_slice *slices = calloc(nworkers,
			sizeof(struct tmul_slice));
	if (slices == NULL) {
		return -1;
	}

	const int r_div = C->nrows / nworkers;
	const int r_rem = C->nrows % nworkers;

	for (int i = 0; i < nworkers; i++) {
		struct tmul_slice *s = &slices[i];

		s->C = C;
		s->A = A;
		s->B = B;

		s->r_off = i * r_div;
		s->r_end = (i + 1) * r_div;
		s->r_end += (i + 1 == nworkers) ? r_rem : 0;

		pthread_create(&s->tid, NULL, tmul_worker, s);
	}

	for (int i = 0; i < nworkers; i++) {
		pthread_join(slices[i].tid, NULL);
	}

	free(slices);

	return 0;
}
