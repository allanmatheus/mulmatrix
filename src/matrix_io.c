#include "matrix.h"

int matrix_from_file(struct matrix *self, FILE *in)
{
	int nrows, ncols;
	int n = fscanf(in, "%d %d", &nrows, &ncols);

	if (n != 2 || matrix_init(self, nrows, ncols) == -1) {
		return -1;
	}

	for (int i = 0; i < nrows; i++) {
		for (int j = 0; j < ncols; j++) {
			n = fscanf(in, "%d", matrix_at(self, i, j));
			if (n != 1) {
				return -1;
			}
		}
	}


	return 0;
}

int matrix_to_file(struct matrix *self, FILE *out)
{
	if (self == NULL) {
		return -1;
	}

	fprintf(out, "%d %d\n", self->nrows, self->ncols);

	for (int i = 0; i < self->nrows; i++) {
		for (int j = 0; j < self->ncols; j++) {
			char *pattern = j + 1 == self->ncols ? "%d\n" : "%d ";
			fprintf(out, pattern, *matrix_at(self, i, j));
		}
	}

	return 0;
}
