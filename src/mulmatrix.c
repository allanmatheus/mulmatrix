/* This is a sequential implementation of matrix multiplication.  It serves as
 * a baseline for speedup comparison with the parallel version.
 *
 * Note: Remember to compare both optimized (the ``-Ol`` flag for compiler
 * auto optimization, where ``l`` is the optimization level in ``[1,2,3,4]``)
 * and non-optimized (``-O0``) versions of this program with the parallel
 * version.
 */
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include "matrix.h"

enum execution_mode {
	SP_MODE = 0,
	MT_MODE,
	MP_MODE,
};

/**
 * usage - Displays help and usage messages.
 */
void usage(const char *progname);

int main(int argc, char **argv)
{
	char *filename_A = NULL;
	char *filename_B = NULL;
	char *filename_C = NULL;

	FILE *input_A = NULL;
	FILE *input_B = NULL;
	FILE *output = stdout;

	int nworkers = 1;
	int mode = SP_MODE;

	int op;
	while ((op = getopt(argc, argv, "ha:b:c:t:p:")) != -1) {
		switch (op) {
		case 'a':
			filename_A = optarg;
			break;

		case 'b':
			filename_B = optarg;
			break;

		case 'c':
			filename_C = optarg;
			break;

		case 't':
			mode = MT_MODE;
			nworkers = atoi(optarg);
			break;

		case 'p':
			mode = MP_MODE;
			nworkers = atoi(optarg);
			break;

		case 'h':
			usage(argv[0]);
			exit(0);
			
		default:
			usage(argv[0]);
			exit(1);
		}
	}

	if (nworkers < 1) {
		fprintf(stderr, "Number of workers can't be less than one\n");
		usage(argv[0]);
		exit(2);
	}

	if (filename_A == NULL || filename_B == NULL) {
		fprintf(stderr, "Must choose input filenames\n");
		usage(argv[0]);
		exit(2);
	}

	input_A = fopen(filename_A, "r");
	if (input_A == NULL) {
		perror("fopen");
		exit(3);
	}

	input_B = fopen(filename_B, "r");
	if (input_B == NULL) {
		perror("fopen");
		fclose(input_A);
		exit(3);
	}

	if (filename_C != NULL) {
		output = fopen(filename_C, "w");
		if (output == NULL) {
			perror("fopen");
			fclose(input_A);
			fclose(input_B);
			exit(3);
		}
	}

	int rt = 0;
	struct matrix A;
	if (matrix_from_file(&A, input_A) == -1) {
		rt = 4;
		fprintf(stderr, "Can't read A\n");
		goto exit;
	}

	struct matrix B;
	if (matrix_from_file(&B, input_B) == -1) {
		rt = 4;
		fprintf(stderr, "Can't read B\n");
		goto exit;
	}

	struct matrix C;
	if (matrix_init(&C, A.nrows, B.ncols) == -1) {
		rt = 4;
		fprintf(stderr, "Can't initialize C\n");
		goto exit;
	}

	int mul_rt = 0;

	switch (mode) {
	case SP_MODE:
		mul_rt = matrix_mul(&C, &A, &B);
		break;

	case MT_MODE:
		mul_rt = matrix_tmul(&C, &A, &B, nworkers);
		break;

	case MP_MODE:
		mul_rt = matrix_pmul(&C, &A, &B, nworkers);
		break;

	default:
		mul_rt = -1;
		fprintf(stderr, "Undefined execution mode\n");
		break;
	}

	if (mul_rt == -1) {
		rt = 4;
		fprintf(stderr, "Can't multiply\n");
		goto exit;
	}

	if (matrix_to_file(&C, output) == -1) {
		rt = 4;
		goto exit;
	}

exit:
	fclose(input_A);
	fclose(input_B);
	fclose(output);

	matrix_free(&A);
	matrix_free(&B);
	matrix_free(&C);

	exit(rt);
}

void usage(const char *progname)
{
	fprintf(stderr, "usage: %s [-h] [-t|-p WORKERS] [-c OUTPUT_FILE]"
			"[-a INPUT_FILE_A] [-b INPUT_FILE_B]\n\n"
			"-h   Shows this help message.\n"
			"-t   Enable Multi-Threading with WORKERS threads.\n"
			"-p   Enable Multi-Processing "
			"     with WORKERS processes.\n"
			"-a   Input file for matrix A.\n"
			"-b   Input file for matrix B.\n"
			"-c   Output file for matrix C=A*B, "
			"     defaults to stdout.\n", progname);
}
